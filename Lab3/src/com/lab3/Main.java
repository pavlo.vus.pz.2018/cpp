package com.lab3;

import org.w3c.dom.Text;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        //Part1--------------------------------------------------------------------------------------------------
        System.out.println("*****PART 1*****");
        File data_file = new File("D:/University/Term 5/CPP/Lab3/data.txt");
        StringBuilder data = new StringBuilder();
        try {
            Scanner input = new Scanner(data_file);
            while (input.hasNextLine()) {
                data.append(input.nextLine()).append("\n");
            }

            System.out.println("Input data:\n"+data);

            input.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        //print all single line sentences separately
        String text = data.toString();
        printAllAndSingleLine(text);

        //find path
        printPathsFromText(text);

        //Part2---------------------------------------------------------------------------------------------------
        System.out.println("*****PART 2*****");
        Scanner in = new Scanner(System.in);
        System.out.println("***Enter text:");
        String inputData = new String();
        if (in.hasNextLine()) {
            inputData = in.nextLine();
        }
        if (inputData.isEmpty()) {
            System.out.println("***Bad input");
            return;
        }

        int n = 0;
        System.out.println("***Enter length of the words: ");
        if (in.hasNextInt()) {
            n = in.nextInt();
        }
        if (n < 1) {
            System.out.println("***Bad input");
            return;
        }

        List<String> interrogativeSentences = findInterrogativeSentences(inputData);
        StringBuilder sentences = new StringBuilder();

        for (String a : interrogativeSentences) {
            System.out.println(a);
            sentences.append(a).append(" ");
        }

        System.out.println("***Found words:\n" + findWordsOfLengthN(n, sentences.toString()));

    }


   public static void printAllAndSingleLine(String text) {
        List<String> sentencesList = new ArrayList<>();
        int firstIndex = 0;
        for (int i = 0; i < text.length(); i++) {

            if (text.charAt(i) == '.' || text.charAt(i) == '!' || text.charAt(i) == '?') {
                sentencesList.add(text.substring(firstIndex, i + 1));

                char next;
                int j = 1;
                next = text.charAt(i + j);
                for (j = 1; (next == ' ' || next == '\n') && i + j < text.length(); j++) {
                    next = text.charAt(i + j);
                }
                firstIndex = i + j - 1;
            }
        }
        System.out.println("***All sentences:\n");
        for (String a : sentencesList) {
            System.out.print(a + "\n\n");
        }
        //single line sentences
       System.out.print("***Single line sentences:\n\n");
       for (String a : sentencesList) {
           if (a.contains("\n")) {
               continue;
           }
           System.out.print(a + "\n\n");
       }

    }

    public static void printPathsFromText(String text) {
        StringBuilder paths = new StringBuilder("***Found paths:\n");

        if (!text.contains(":\\")) {
            paths.append("***Not found\n");
            System.out.print(paths);
            return;
        }

        int leftIndex;
        int lastIndex;

        while (text.contains(":\\")) {
            //find left side
            leftIndex = text.indexOf(":\\");
            while (text.charAt(leftIndex) != ' ') {
                leftIndex--;
            }
            text = text.substring(leftIndex + 1);
            //find right side
            lastIndex = text.indexOf(":\\") + 1;
            while (text.charAt(lastIndex) != ' ') {
                lastIndex++;
            }
            paths.append(text.substring(0, lastIndex) + "\n");
            text = text.substring(lastIndex);
        }
        System.out.print(paths);
    }

    public static List findInterrogativeSentences(String text) {
        if (text.isEmpty()) {
            return null;
        }

        List<String> sentences = new ArrayList<>();
        Pattern pattern = Pattern.compile("[A-zА-яі\\'0-9]+[A-zА-яі\\'0-9\\s\\,]*\\?");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            sentences.add(matcher.group());
        }
        return sentences;
    }

    public static List findWordsOfLengthN(int n, String text) {
        if (text.isEmpty()) {
            return null;
        }
        List<String> words = new ArrayList<>();

        Pattern pattern = Pattern.compile("(?<=\\s)[A-zА-яі\\']{" + n + "}(?=[\\s\\?\\,])");
        Matcher matcher = pattern.matcher(" " + text);
        while (matcher.find()) {
            if (words.contains(matcher.group())) {
                continue;
            }
            words.add(matcher.group());
        }

        return words;
    }
}

