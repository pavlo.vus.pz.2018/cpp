package com.lab3;

import org.junit.Assert;
import org.junit.Test;

public class Lab3Test {
    @Test
    public void TestFindInterrogative() {
        String data = "Some simple text! Щось треба? Another text. Another text with some question? Ok?";
        String expected = "[Щось треба?, Another text with some question?, Ok?]";
        Assert.assertEquals(expected, Main.findInterrogativeSentences(data).toString());
    }

    @Test
    public void TestFindWordsOfLength4() {
        int length = 4;
        String data = "Щось треба?, Another text with some question?, Ok?";
        String expected = "[Щось, text, with, some]";
        Assert.assertEquals(expected, Main.findWordsOfLengthN(length, data).toString());
    }

    @Test
    public void TestFindWordsOfLength0() {
        int length = 0;
        String data = "Щось треба?, Another text with some question?, Ok?";
        String expected = "[]";
        Assert.assertEquals(expected, Main.findWordsOfLengthN(length, data).toString());
    }

    @Test
    public void TestFindWordsOfLength10() {
        int length = 10;
        String data = "Щось треба?, Another text with some question?, Ok?";
        String expected = "[]";
        Assert.assertEquals(expected, Main.findWordsOfLengthN(length, data).toString());
    }
}
